package com.minecraftonline.villagecollectiontest;

import net.minecraft.util.math.BlockPos;

import java.util.List;

public interface VillageCollectionBridge {
    List<BlockPos> bridge$getVillagerPositionsList();
    int bridge$getVillagerEnqueueAttempts();
}
