package com.minecraftonline.villagecollectiontest;


import com.google.inject.Inject;
import net.minecraft.village.VillageCollection;
import net.minecraft.world.WorldServer;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

@Plugin(id = "villagecollectiontest", description = "See state of VillageCollection.villagerPositionsList")
public class VillagerCollectionTest {
    @Inject
    private Logger logger;

    @Listener
    public void onServerStart(GameStartingServerEvent event) {
        logger.info("village collection variable viewing plugin thing is active");
        registerCommands();
    }

    private void registerCommands() {
        CommandSpec villagerPositionsCommandSpec = CommandSpec.builder()
                .description(Text.of("View state of VillageCollection.villagerPositionsList variable"))
                .permission("villagecollectiontest.command.villagerpositions")
                .executor(new VillagerPositionsCommand())
                .build();
        Sponge.getCommandManager().register(this, villagerPositionsCommandSpec, "villagerpositions");

        CommandSpec villagerEnqueueAttemptsCommandSpec = CommandSpec.builder()
                .description(Text.of("View state how many villagers attempted to add to VillageCollection.villagerPositionsList last tick"))
                .permission("villagecollectiontest.command.villagerenqueueattempts")
                .executor((src, args) -> {
                    if (src instanceof Player) {
                        Player player = (Player) src;
                        org.spongepowered.api.world.World spongeWorld = player.getWorld();
                        if (!(spongeWorld instanceof WorldServer)) {
                            src.sendMessage(Text.of(TextColors.RED, "World you are in is not an instance of WorldServer!"));
                            return CommandResult.success();
                        }
                        WorldServer world = (WorldServer) spongeWorld;
                        VillageCollection villageCollection = world.getVillageCollection();
                        if (!(villageCollection instanceof VillageCollectionBridge)) {
                            src.sendMessage(Text.of(TextColors.RED, "World you are in is not an instance of VillageCollectionBridge!"));
                            return CommandResult.success();
                        }
                        VillageCollectionBridge villageCollectionBridge = (VillageCollectionBridge) villageCollection;
                        src.sendMessage(Text.of(villageCollectionBridge.bridge$getVillagerEnqueueAttempts() + " villagers attempted to add to villagerPositionsList last tick"));
                        return CommandResult.success();
                    }
                    return CommandResult.empty();
                })
                .build();
        Sponge.getCommandManager().register(this, villagerEnqueueAttemptsCommandSpec, "villagerenqueueattempts");
    }

}
