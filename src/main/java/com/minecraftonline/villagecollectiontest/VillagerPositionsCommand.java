package com.minecraftonline.villagecollectiontest;

import net.minecraft.village.VillageCollection;
import net.minecraft.world.WorldServer;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

public class VillagerPositionsCommand implements CommandExecutor {

    @NonNull
    @Override
    public CommandResult execute(@NonNull CommandSource src,@NonNull CommandContext args) {
        if (src instanceof Player) {
            Player player = (Player) src;
            org.spongepowered.api.world.World spongeWorld = player.getWorld();
            if (!(spongeWorld instanceof WorldServer)) {
                src.sendMessage(Text.of(TextColors.RED, "World you are in is not an instance of WorldServer!"));
                return CommandResult.success();
            }
            WorldServer world = (WorldServer) spongeWorld;
            VillageCollection villageCollection = world.getVillageCollection();
            if (!(villageCollection instanceof VillageCollectionBridge)) {
                src.sendMessage(Text.of(TextColors.RED, "World you are in is not an instance of VillageCollectionBridge!"));
                return CommandResult.success();
            }
            VillageCollectionBridge villageCollectionBridge = (VillageCollectionBridge) villageCollection;
            if (villageCollectionBridge.bridge$getVillagerPositionsList() != null) {
                int length = villageCollectionBridge.bridge$getVillagerPositionsList().size();
                if (length >= 65) {
                    src.sendMessage(Text.of(TextColors.RED, "Length of villagerPositions: " + length));
                }
                else {
                    src.sendMessage(Text.of(TextColors.GREEN, "Length of villagerPositions: " + length));
                }
                src.sendMessage(Text.of("Value of villagerPositions: " + villageCollectionBridge.bridge$getVillagerPositionsList()));
            }
            else {
                src.sendMessage(Text.of(TextColors.RED, "villagerPositions is null"));
            }
            return CommandResult.success();
        }
        return CommandResult.empty();
    }
}
