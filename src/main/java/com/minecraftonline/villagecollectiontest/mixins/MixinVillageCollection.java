package com.minecraftonline.villagecollectiontest.mixins;

import com.minecraftonline.villagecollectiontest.VillageCollectionBridge;
import net.minecraft.util.math.BlockPos;
import net.minecraft.village.VillageCollection;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.gen.Accessor;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.List;

@Mixin(VillageCollection.class)
public abstract class MixinVillageCollection implements VillageCollectionBridge {

    @Accessor
    public abstract List<BlockPos> getVillagerPositionsList();

    @Override
    public int bridge$getVillagerEnqueueAttempts() {
        return villageCollectionTest$villagerEnqueueAttempts;
    }

    @Override
    public List<BlockPos> bridge$getVillagerPositionsList() {
        return getVillagerPositionsList();
    }

    @Unique
    private int villageCollectionTest$totalVillagerEnqueueAttempts = 0;
    @Unique
    private int villageCollectionTest$totalVillagerEnqueueAttemptsLast = 0;
    @Unique
    private int villageCollectionTest$villagerEnqueueAttempts = 0;

    @Inject(at = @At("HEAD"), method = "tick()V")
    public void tick(CallbackInfo ci) {
        villageCollectionTest$villagerEnqueueAttempts = villageCollectionTest$totalVillagerEnqueueAttempts - villageCollectionTest$totalVillagerEnqueueAttemptsLast;
        villageCollectionTest$totalVillagerEnqueueAttemptsLast = villageCollectionTest$totalVillagerEnqueueAttempts;
    }

    @Inject(at = @At("HEAD"), method = "addToVillagerPositionList")
    public void addToVillagerPositionList(BlockPos pos, CallbackInfo ci) {
        ++villageCollectionTest$totalVillagerEnqueueAttempts;
    }
}